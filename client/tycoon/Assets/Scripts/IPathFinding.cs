﻿
public interface IPathFinding<TLocation> where TLocation : struct
{
    INode<TLocation> FindPath(IMap<TLocation> map, INode<TLocation> startNode, INode<TLocation> targetNode, int maxIterateCount);
    void ProcessSuccessor(INode<TLocation> node, INode<TLocation> successor);
}

public interface INode<TLocation> where TLocation : struct
{
    TLocation Location { get; set; }
    INode<TLocation> PreviousNode { get; set; }
    int F { get; set; }
    int G { get; set; }
    int H { get; set; }
    bool CompareLocation(INode<TLocation> target);
    bool CompareLocation(TLocation location);
    int Distance(INode<TLocation> target);
}

public interface IMap<TLocation> where TLocation : struct
{
    void BrowserSuccessors(INode<TLocation> sourceNode, IPathFinding<TLocation> pathFinding);
    int GetSuccessorDistance(INode<TLocation> node, INode<TLocation> successor);
}
