﻿using System;
using System.Collections.Generic;

public struct Location2D
{
    public int RowIndex;
    public int ColIndex;

    public Location2D(int rowIndex, int colIndex)
    {
        RowIndex = rowIndex;
        ColIndex = colIndex;
    }

    public bool Compare(Location2D location)
    {
        return RowIndex == location.RowIndex 
               && ColIndex == location.ColIndex;
    }
}

public class Default2DMap : IMap<Location2D>
{
    private readonly bool[,] _data;
    private readonly int _maxRowIndex;
    private readonly int _maxColIndex;

    public Default2DMap(bool[,] data)
    {
        _data = data;
        _maxRowIndex = _data.GetLength(0) - 1;
        _maxColIndex = _data.GetLength(1) - 1;
    }

    public virtual bool CanGoToSuccessor(Location2D origin, Location2D successor)
    {
        return _data[successor.RowIndex, successor.ColIndex] &&
               (origin.RowIndex == successor.RowIndex || origin.ColIndex == successor.ColIndex);
    }

    public int GetMaxRowIndex()
    {
        return _maxRowIndex;
    }

    public int GetMaxColumnIndex()
    {
        return _maxColIndex;
    }

    public void BrowserSuccessors(INode<Location2D> sourceNode, IPathFinding<Location2D> pathFinding)
    {
        Node node = sourceNode as Node;
        if (node == null)
        {
            return;
        }

        int maxRowIndex = GetMaxRowIndex();
        int maxColIndex = GetMaxColumnIndex();

        int fromRowIndex = node.Location.RowIndex <= 0 ? 0 : node.Location.RowIndex - 1;
        int toRowIndex = node.Location.RowIndex == maxRowIndex ? maxRowIndex : node.Location.RowIndex + 1;

        int fromColIndex = node.Location.ColIndex <= 0 ? 0 : node.Location.ColIndex - 1;
        int toColIndex = node.Location.ColIndex == maxColIndex ? maxColIndex : node.Location.ColIndex + 1;

        for (int colIndex = fromColIndex; colIndex <= toColIndex; colIndex++)
        {
            for (int rowIndex = fromRowIndex; rowIndex <= toRowIndex; rowIndex++)
            {
                Location2D successorLocation = new Location2D(rowIndex, colIndex);
                if (CanGoToSuccessor(node.Location, successorLocation))
                {
                    Node successor = new Node(successorLocation);
                    pathFinding.ProcessSuccessor(node, successor);
                }
            }
        }
    }

    public virtual int GetSuccessorDistance(INode<Location2D> node, INode<Location2D> successor)
    {
        return 1;
    }
}

public class Node : INode<Location2D>
{
    public Location2D Location { get; set; }
    public INode<Location2D> PreviousNode { get; set; }
        
    public int F { get; set; }
    public int G { get; set; }
    public int H { get; set; }

    public Node(Location2D location)
    {
        Location = location;
            
        F = 0;
        G = 0;
        H = 0;
    }

    public Location2D[] GetPath()
    {
        Stack<Location2D> path = new Stack<Location2D>();

        Node currentNode = this;
        while (currentNode != null)
        {
            path.Push(currentNode.Location);
            currentNode = currentNode.PreviousNode as Node;
        }

        return path.ToArray();
    }

    public bool CompareLocation(INode<Location2D> target)
    {
        Node targetNode = target as Node;

        if (targetNode == null)
        {
            return false;
        }

        return CompareLocation(target.Location);
    }

    public bool CompareLocation(Location2D location)
    {
        return Location.Compare(location);
    }

    public int Distance(INode<Location2D> target)
    {
        Node targetNode = target as Node;

        if (targetNode == null)
        {
            return int.MaxValue;
        }
            
        return Math.Abs(targetNode.Location.RowIndex - Location.RowIndex)
               + Math.Abs(targetNode.Location.ColIndex - Location.ColIndex);
    }
}
