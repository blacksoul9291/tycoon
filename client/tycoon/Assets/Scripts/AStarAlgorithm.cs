﻿using System;
using System.Collections.Generic;

public class AStarAlgorithm<TLocation> : IPathFinding<TLocation> where TLocation : struct
{
    private readonly OpenList<TLocation> _open = new OpenList<TLocation>();
    private readonly Dictionary<TLocation, INode<TLocation>> _closed = new Dictionary<TLocation, INode<TLocation>>();
    private IMap<TLocation> _map;
    private INode<TLocation> _targetNode;

    private class OpenList<TLocation> where TLocation : struct
    {
        private INode<TLocation> _lowestFNode;
        private readonly Dictionary<TLocation, INode<TLocation>> _dictionary = new Dictionary<TLocation, INode<TLocation>>();

        public int Count
        {
            get
            {
                return _dictionary.Count;
            }
        }

        public INode<TLocation> GetAndRemoveLowestFNode()
        {
            INode<TLocation> currentLowestFNode = _lowestFNode;
            Remove(currentLowestFNode.Location);

            return currentLowestFNode;
        }

        private static INode<TLocation> FindLowestFNode(Dictionary<TLocation, INode<TLocation>> dictionary)
        {
            INode<TLocation> lowestFNode = null;
            foreach (var keyValue in dictionary)
            {
                if (lowestFNode == null || keyValue.Value.F < lowestFNode.F)
                {
                    lowestFNode = keyValue.Value;
                }
            }

            return lowestFNode;
        }

        public void Add(INode<TLocation> node)
        {
            if (_lowestFNode == null || node.F < _lowestFNode.F)
            {
                _lowestFNode = node;
            }

            _dictionary[node.Location] = node;
        }

        private void Remove(TLocation location)
        {
            if (_dictionary.TryGetValue(location, out INode<TLocation> node))
            {
                int f = node.F;
                _dictionary.Remove(location);
                if (f <= _lowestFNode.F)
                {
                    _lowestFNode = FindLowestFNode(_dictionary);
                }
            }
        }

        public bool Contains(TLocation location, out INode<TLocation> node)
        {
            return _dictionary.TryGetValue(location, out node);
        }

        public void Clear()
        {
            _lowestFNode = null;
            _dictionary.Clear();
        }
    }

    public INode<TLocation> FindPath(IMap<TLocation> map, INode<TLocation> startNode, INode<TLocation> targetNode, int maxIterateCount)
    {
        _map = map;
        _open.Clear();
        _closed.Clear();
        _targetNode = targetNode;

        _open.Add(startNode);

        int count = 0;

        while (_open.Count > 0 && count < maxIterateCount)
        {
            count++;
            INode<TLocation> node = _open.GetAndRemoveLowestFNode();
            
            if (node == null)
            {
                break;
            }

            if (node.CompareLocation(targetNode))
            {
                return node;
            }

            _map.BrowserSuccessors(node, this);
            
            _closed[node.Location] = node;
        }

        return null;
    }

    public void ProcessSuccessor(INode<TLocation> node, INode<TLocation> successor)
    {
        successor.PreviousNode = node;

        int distance = _map.GetSuccessorDistance(node, successor);

        successor.G = node.G + distance;
        successor.H = successor.Distance(_targetNode);
        successor.F = successor.G + successor.H;

        if (_open.Contains(successor.Location, out INode<TLocation> openNode) && openNode.F < successor.F)
        {
            return;
        }
        
        if(_closed.TryGetValue(successor.Location, out INode<TLocation> closedNode))
        {
            if (closedNode.F < successor.F)
            {
                return;
            }
            _closed.Remove(successor.Location);
        }

        _open.Add(successor);
    }
}