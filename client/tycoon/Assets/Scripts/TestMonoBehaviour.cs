﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMonoBehaviour : MonoBehaviour
{
    public class TestMap : Default2DMap
    {
        public override int GetSuccessorDistance(INode<Location2D> node, INode<Location2D> successor)
        {
            if (node.CompareLocation(new Location2D(1, 2)))
            {
                return 100;
            }
            
            if (node.CompareLocation(new Location2D(3, 4)))
            {
                return 100;
            }

            return 1;
        }

        public TestMap(bool[,] data) : base(data)
        {
            
        }
    }
    void Start()
    {
        AStarAlgorithm<Location2D> algorithm = new AStarAlgorithm<Location2D>();
        
        bool[,] dataMap = new [,]
        {
            {true, true,   true,  true,   true,   true},
            {true, false,  true,  false,  false,  true},
            {true, false,  true,  false,  false,  true},
            {true, true,   true,  true,   true,   true},
        };
        
        IMap<Location2D> map = new TestMap(dataMap);

        Node start = new Node(new Location2D(3, 0));
        Node target = new Node(new Location2D(3, 5));
        INode<Location2D> finalNode = algorithm.FindPath(map, start, target, 10000);

        PrintMap(dataMap, (finalNode as Node).GetPath());
    }
    
    private void PrintMap(bool[,] map, Location2D[] path)
    {
        string content = string.Empty;
        for (int rowIndex = 0, width = map.GetLength(0); rowIndex < width; rowIndex++)
        {
            for (int colIndex = 0, height = map.GetLength(1); colIndex < height; colIndex++)
            {
                if (map[rowIndex, colIndex] == false)
                {
                    content += " + ";
                    continue;
                }

                if (DoesPathContainsIndex(rowIndex, colIndex, path, out int pathIndex))
                {
                    content += string.Format(" {0} ", pathIndex);
                    continue;
                }

                content += " o ";

            }

            content += "\n";
        }
        
        Debug.Log(content);
    }
    
    private bool DoesPathContainsIndex(int rowIndex, int colIndex, Location2D[] path, out int index)
    {
        index = 0;
        for (int i = 0, length = path.Length; i < length; i++)
        {
            if (path[i].RowIndex == rowIndex && path[i].ColIndex == colIndex)
            {
                index = i;
                return true;
            }
        }
        
        return false;
    }
    
    
}
