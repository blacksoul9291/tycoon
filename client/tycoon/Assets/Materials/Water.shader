﻿Shader "Unlit/Water"
{
    Properties
    {
        _WaterColor ("Water Color", Color) = (1,1,1,1)
        _WaveColor ("Wave Color", Color) = (1,1,1,1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
                fixed4 color : COLOR;
            };

            fixed4 _WaterColor;
            fixed4 _WaveColor;

            v2f vert (appdata v) 
            {
                v2f o;
                v.vertex.y = 0.5 * (sin(10 * _Time + 3 * v.vertex.x) - sin(10 * _Time + 3 * v.vertex.z));
                o.color = float4(0, 0, 10, 1) - v.vertex;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                //float3 dir = WorldSpaceViewDir(v.vertex);
                //o.color.x = dir.x;
                //o.color.y = dir.y;
                //o.color.z = dir.z;
                
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = _WaterColor;
                
                col.r =  0;
                col.g =  i.color.g;
                col.b =  0;

                
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
