﻿Shader "Unlit/NMWater"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _WaterColor ("Wate rColor", Color) = (1,1,1,1)
    }
    SubShader
    {
        Tags { "Queue"="Transparent" "RenderType"="Transparent" }
        LOD 100
        
        ZWrite OFF
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
                fixed4 color : COLOR;
                float height : HEIGHT;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            fixed4 _WaterColor;

            v2f vert (appdata v)
            {
                v2f o;
                
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.color = tex2Dlod(_MainTex, float4(v.uv, 0, 0));
                if(o.color.y > 0.5)
                {
                    o.height = 0.1 * (sin(50 * _Time + v.vertex.x) + sin(50 * _Time + v.vertex.z)) + o.color.y * 0.5;
                }
                else
                {
                    o.height = 0.1 * (sin(50 * _Time + v.vertex.x) + sin(50 * _Time + v.vertex.z)) - o.color.y * 0.5;
                }

                v.vertex.y = o.height;
                o.vertex = UnityObjectToClipPos(v.vertex);
               
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o; 
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 tx = tex2D(_MainTex, i.uv += _Time * 0.1);
                fixed4 col = tx + i.height * 2;
                
                if(col.y < 0.2 || col.y > 0.8)
                {
                    col = tx;
                }
                else
                {
                    
                    col = 0.75 - tx;
                }
                

                col += _WaterColor;
                
                col.a = 0.6;
                
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
